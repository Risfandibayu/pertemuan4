package bayu.risfandi.pertemuan4

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapter(val context: Context, arrayList: ArrayList<HashMap<String, Any>>) : BaseAdapter() {

    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    var list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txtFileName : TextView? = null
        var txtFileType : TextView? = null
        var txtFileUrl : TextView? = null
        var img : ImageView? = null
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if(convertView == null){
            var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_data,null,true)
            holder.txtFileName = view!!.findViewById(R.id.textView_fileName) as TextView
            holder.txtFileType = view!!.findViewById(R.id.textView_Type) as TextView
            holder.txtFileUrl = view!!.findViewById(R.id.textView_url) as TextView
            holder.img = view!!.findViewById(R.id.imgV) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }

        var fileType = list.get(position).get(F_TYPE).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txtFileName!!.setText(list.get(position).get(F_NAME).toString())
        holder.txtFileType!!.setText(fileType)
        holder.txtFileUrl!!.setText(uri.toString())
        holder.txtFileUrl!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(holder.txtFileUrl!!.text.toString()))
            context.startActivity(intent)
        }

        when(fileType){
            ".pdf" -> {holder.img!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
            ".docx" -> {holder.img!!.setImageResource(android.R.drawable.ic_menu_edit)}
            ".mp4" -> {holder.img!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {
                Picasso.get().load(uri).into(holder.img)}
        }

        return  view!!
    }
}